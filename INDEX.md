# BladeEnc

Blade's MP3 Encoder generates MP3 files from WAV/AIFF/RAW data

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## BLADEENC.LSM

<table>
<tr><td>title</td><td>BladeEnc</td></tr>
<tr><td>version</td><td>0.94.1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-08-16</td></tr>
<tr><td>description</td><td>Generate MP3 files from WAV/AIFF/RAW</td></tr>
<tr><td>keywords</td><td>mp3, encoder</td></tr>
<tr><td>author</td><td>Matt Craven</td></tr>
<tr><td>original&nbsp;site</td><td>http://freshmeat.sourceforge.net/projects/bladeenc/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU Lesser General Public License, Version 2.1</td></tr>
<tr><td>summary</td><td>Blade's MP3 Encoder generates MP3 files from WAV/AIFF/RAW data</td></tr>
</table>
