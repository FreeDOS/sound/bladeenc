#include <windows.h>

#define _BLADEDLL

#include "codec.h"
#include "bladedll.h"



/************ Version *************/

#define		DLL_MAJOR_VERSION		0x01
#define		DLL_MINOR_VERSION		0x01

#define		ENCODER_MAJOR_VERSION	0x00
#define		ENCODER_MINOR_VERSION	0x5E		// 94

#define		RELEASE_DAY				8
#define		RELEASE_MONTH			3
#define		RELEASE_YEAR			2001
	
const char	BladeEncHomepage[BE_MAX_HOMEPAGE + 1] = "http://bladeenc.mp3.no";	

/************ MP3 Variables *************/

DWORD	MP3_AllowedSamplerate[]	= { 48000, 44100, 32000	};
WORD	MP3_AllowedBitrate[]	= { 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320 };

#define	MP3_SAMPLERATES			(sizeof(MP3_AllowedSamplerate)/sizeof(DWORD))
#define	MP3_BITRATES			(sizeof(MP3_AllowedBitrate)/sizeof(WORD))

/****************************************/

// Only one handle is supported, this is the only valid handle value

#define		BE_EMPTY_HANDLE_VALUE		0
#define		BE_VALID_HANDLE_VALUE		1

HBE_STREAM	StreamHandle = BE_EMPTY_HANDLE_VALUE;

/****************************************/

BOOL WINAPI DllMain(HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{   
	switch(dwReason)	{

		case DLL_PROCESS_ATTACH:

			break;
		
		case DLL_THREAD_ATTACH:

			break;
		
		case DLL_THREAD_DETACH:

			break;

		case DLL_PROCESS_DETACH:

			break;
	}    
	
	return TRUE;
}

__declspec(dllexport) VOID beVersion(PBE_VERSION pbeVersion)
{
	pbeVersion->byDLLMajorVersion	= DLL_MAJOR_VERSION;
	pbeVersion->byDLLMinorVersion	= DLL_MINOR_VERSION;

	pbeVersion->byMajorVersion		= ENCODER_MAJOR_VERSION;
	pbeVersion->byMinorVersion		= ENCODER_MINOR_VERSION;

	pbeVersion->byDay				= RELEASE_DAY;
	pbeVersion->byMonth				= RELEASE_MONTH;
	pbeVersion->wYear				= RELEASE_YEAR;

	lstrcpy(pbeVersion->zHomepage, BladeEncHomepage);
}

__declspec(dllexport) BE_ERR beInitStream(PBE_CONFIG pbeConfig, PDWORD dwSamples, PDWORD dwBufferSize, PHBE_STREAM phbeStream)
{
	int				i;
	CodecInitIn		In;
	CodecInitOut	*pOut;

	// Check if there are handles available (only one handle is supported)

	if(StreamHandle != BE_EMPTY_HANDLE_VALUE)	{

		return BE_ERR_NO_MORE_HANDLES;
	}
	
	// Check for supported encoding format

	if(pbeConfig->dwConfig != BE_CONFIG_MP3)	{

		return BE_ERR_INVALID_FORMAT;
	}

	// Check for valid sample rate 

	for(i = 0; i < MP3_SAMPLERATES; i++)	{		

		if(pbeConfig->format.mp3.dwSampleRate == MP3_AllowedSamplerate[i])
			break;
	}

	// Valid sample rate were found? 

	if(i == MP3_SAMPLERATES)	{
		
		return BE_ERR_INVALID_FORMAT_PARAMETERS;
	}

	// Check for valid bitrate

	for(i = 0; i < MP3_BITRATES; i++)	{

		if(pbeConfig->format.mp3.wBitrate == MP3_AllowedBitrate[i])
			break;
	}

	// Valid bitrate found?

	if(i == MP3_BITRATES)	{

		return BE_ERR_INVALID_FORMAT_PARAMETERS;
	}

	// Check for valid mode 

	if( pbeConfig->format.mp3.byMode != BE_MP3_MODE_STEREO &&
		pbeConfig->format.mp3.byMode != BE_MP3_MODE_DUALCHANNEL &&
		pbeConfig->format.mp3.byMode != BE_MP3_MODE_MONO)	{
		
		return BE_ERR_INVALID_FORMAT_PARAMETERS;
	}
			
	// Initialize Stream

	In.frequency	= (int) pbeConfig->format.mp3.dwSampleRate;
	In.mode			= (int) pbeConfig->format.mp3.byMode;
	In.bitrate		= (int) pbeConfig->format.mp3.wBitrate;
	In.emphasis		= 0;
	In.fPrivate		= pbeConfig->format.mp3.bPrivate ? 1 : 0;
	In.fCRC			= pbeConfig->format.mp3.bCRC ? 1 : 0;
	In.fCopyright	= pbeConfig->format.mp3.bCopyright ? 1 : 0;
	In.fOriginal	= pbeConfig->format.mp3.bOriginal ? 1 : 0;

	pOut = codecInit(&In);

	*dwSamples = (DWORD) pOut->nSamples;
	*dwBufferSize = (DWORD) pOut->bufferSize;
	*phbeStream = BE_VALID_HANDLE_VALUE;

	StreamHandle = BE_VALID_HANDLE_VALUE;

	return BE_ERR_SUCCESSFUL;	
}

__declspec(dllexport) BE_ERR beEncodeChunk(HBE_STREAM hbeStream, DWORD nSamples, PSHORT pSamples, PBYTE pOutput, PDWORD pdwOutput)
{
	// Check for valid handle

	if(hbeStream != BE_VALID_HANDLE_VALUE)	{

		return BE_ERR_INVALID_HANDLE ;
	}

	// Encode the chunk

	*pdwOutput = codecEncodeChunk((int)nSamples, pSamples, pOutput);

	return BE_ERR_SUCCESSFUL;
}

__declspec(dllexport) BE_ERR beDeinitStream(HBE_STREAM hbeStream, PBYTE pOutput, PDWORD pdwOutput)
{	
	// Check for valid handle

	if(hbeStream != BE_VALID_HANDLE_VALUE)	{

		return BE_ERR_INVALID_HANDLE ;
	}

	// Exit/Close stream

	*pdwOutput = codecExit(pOutput);
		
	return BE_ERR_SUCCESSFUL;
}

__declspec(dllexport) BE_ERR beCloseStream(HBE_STREAM hbeStream)
{
	// Check for valid handle

	if(hbeStream != BE_VALID_HANDLE_VALUE)	{

		return BE_ERR_INVALID_HANDLE ;
	}

	// Close stream	

	StreamHandle = BE_EMPTY_HANDLE_VALUE;	
	
	return BE_ERR_SUCCESSFUL;
}
