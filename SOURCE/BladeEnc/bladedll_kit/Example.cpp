#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "bladedll.h"

BEINITSTREAM	beInitStream;
BEENCODECHUNK	beEncodeChunk;
BEDEINITSTREAM	beDeinitStream;
BECLOSESTREAM	beCloseStream;
BEVERSION		beVersion;

int main(int argc, char *argv[])
{	
	if(argc != 2)	{

		printf("Syntax: %s <filename.raw>", argv[0]);
		return -1;
	}

	// Load BladeEnc.DLL

	HINSTANCE hBladeDLL = LoadLibrary("BLADEENC.DLL");

	if(!hBladeDLL)	{

		printf("Error loading BLADEENC.DLL");
		return -1;
	}

	// Get Interface

	beInitStream	= (BEINITSTREAM) GetProcAddress(hBladeDLL, TEXT_BEINITSTREAM);
	beEncodeChunk	= (BEENCODECHUNK) GetProcAddress(hBladeDLL, TEXT_BEENCODECHUNK);
	beDeinitStream	= (BEDEINITSTREAM) GetProcAddress(hBladeDLL, TEXT_BEDEINITSTREAM);
	beCloseStream	= (BECLOSESTREAM) GetProcAddress(hBladeDLL, TEXT_BECLOSESTREAM);
	beVersion		= (BEVERSION) GetProcAddress(hBladeDLL, TEXT_BEVERSION);

	if(!beInitStream || !beEncodeChunk || !beDeinitStream || !beCloseStream || !beVersion)	{

		printf("Unable to get BladeEnc interface");
		return -1;
	}

	// Get Version

	BE_VERSION	Version;

	beVersion(&Version);

	printf( "BladeEnc.dll version %u.%02u (%u/%u/%u)\n"
			"BladeEnc Engine %u.%02u\n"
			"BladeEnc homepage at %s\n\n",	
			Version.byDLLMajorVersion, Version.byDLLMinorVersion,
			Version.byDay, Version.byMonth, Version.wYear,
			Version.byMajorVersion, Version.byMinorVersion,
			Version.zHomepage);

	// Open RAW file
	
	int hIn = open(argv[1], O_RDONLY | O_BINARY);

	if(hIn == -1)	{

		printf("Error opening %s", argv[1]);
		return -1;
	}

	// Generate filename.mp3

	char zOutputFilename[MAX_PATH + 1];	
	lstrcpy(zOutputFilename, argv[1]);
	int l = lstrlen(zOutputFilename);
	while(l && zOutputFilename[l] != '.')	{

		l--;
	}

	if(!l)	{

		l = lstrlen(zOutputFilename) - 1;
	}

	zOutputFilename[l] = '\0';

	lstrcat(zOutputFilename, ".mp3");

	// Open MP3 file

	int hOut = open(zOutputFilename, O_WRONLY | O_BINARY | O_TRUNC | O_CREAT, S_IWRITE);

	if(hOut == -1)	{

		printf("Error creating file %s", zOutputFilename);
		return -1;
	}

	// Open MP3 Encoding Stream

	BE_CONFIG	beConfig;

	beConfig.dwConfig = BE_CONFIG_MP3;

	beConfig.format.mp3.dwSampleRate	= 44100;
	beConfig.format.mp3.byMode			= BE_MP3_MODE_STEREO;
	beConfig.format.mp3.wBitrate		= 128;
	beConfig.format.mp3.bCopyright		= FALSE;
	beConfig.format.mp3.bCRC			= FALSE;
	beConfig.format.mp3.bOriginal		= FALSE;
	beConfig.format.mp3.bPrivate		= FALSE;

	DWORD		dwSamples, dwMP3Buffer;
	HBE_STREAM	hbeStream;
	BE_ERR		err;

	err = beInitStream(&beConfig, &dwSamples, &dwMP3Buffer, &hbeStream);

	if(err != BE_ERR_SUCCESSFUL)	{

		printf("Error opening encoding stream (%lu)", err);
		return -1;
	}

	// Allocate buffers

	PBYTE pMP3Buffer = new BYTE[dwMP3Buffer];

	PSHORT pBuffer = new SHORT[dwSamples];

	if(!pMP3Buffer || !pBuffer)	{

		printf("Out of memory");
		return -1;
	}

	// Start encoding

	DWORD	length = filelength(hIn);
	DWORD	done = 0;
	DWORD	dwWrite;
	DWORD	toread;
	DWORD	towrite;

	setbuf(stdout,NULL);

	while(done < length)	{

		if(done + dwSamples * 2 < length)	{

			toread = dwSamples * 2;
		}
		else	{

			toread = length - done;
		}

		if(read(hIn, pBuffer, toread) == -1)	{

			printf("\nRead error");
			return -1;
		}		 

		err = beEncodeChunk(hbeStream, toread/2, pBuffer, pMP3Buffer, &towrite);

		if(err != BE_ERR_SUCCESSFUL)	{

			beCloseStream(hbeStream);
			printf("beEncodeChunk() failed (%lu)", err);
			return -1;
		}
		
		if(write(hOut, pMP3Buffer, towrite) == -1)	{

			printf("\nWrite error");
			return -1;
		}

		done += toread;

		printf("Done: %0.2f%%     \r", 100 * (float)done/(float)length);	
	}

	err = beDeinitStream(hbeStream, pMP3Buffer, &dwWrite);

	if(err != BE_ERR_SUCCESSFUL)	{

		beCloseStream(hbeStream);
		printf("beExitStream failed (%lu)", err);
		return -1;
	}

	if(dwWrite)	{

		if(write(hOut, pMP3Buffer, dwWrite) == -1)	{

			printf("Write error");
			return -1;
		}
	}

	beCloseStream(hbeStream);

	close(hIn);
	close(hOut);
	
	printf("\n\nIt's Easy!");

	return 0;
}

